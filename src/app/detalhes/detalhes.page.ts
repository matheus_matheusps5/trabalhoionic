import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BolosService } from '../services/bolos.service';
import { Bolo } from '../intefaces/bolo';
import { ToastController, LoadingController } from '@ionic/angular';



@Component({
  selector: 'app-detalhes',
  templateUrl: './detalhes.page.html',
  styleUrls: ['./detalhes.page.scss'],
})

export class DetalhesPage implements OnInit {
  id: number;
  bolo: Bolo = {};

  constructor(private activaveRoute: ActivatedRoute,
    private boloService: BolosService) { }

  ngOnInit() {
      this.id = this.activaveRoute.snapshot.params["id"];
      this.getBolo();
  }

  getBolo(){
    this.boloService.getBolos().subscribe((data)=>{
      this.bolo = data[this.id - 1];
    });
  }



}