import { Component, OnInit } from '@angular/core';
import { LoadingController, ToastController, NavController } from '@ionic/angular';
import { BolosService } from '../services/bolos.service';
import { ActivatedRoute } from '@angular/router';
import { Bolo } from '../intefaces/bolo';
import { dismiss } from '@ionic/core/dist/types/utils/overlays';

@Component({
  selector: 'app-pedido',
  templateUrl: './pedido.page.html',
  styleUrls: ['./pedido.page.scss'],
})
export class PedidoPage implements OnInit {

  id: number;
  bolo: Bolo = {};
  loading: any;

  constructor(
    private activaveRoute: ActivatedRoute,
    private loadingController: LoadingController,
    private toastController: ToastController,
    private boloService: BolosService,
    private navCtrl: NavController
    )
    {}

  ngOnInit() {
      this.id = this.activaveRoute.snapshot.params["id"];
      this.getBolo();
  }

  getBolo(){
    this.boloService.getBolos().subscribe((data)=>{
      this.bolo = data[this.id - 1];
    });
  }

  async confirma(){
    await this.presentLoading();
    setTimeout(() => {
      this.loading.dismiss().then(()=>{
        this.navCtrl.navigateRoot('home');
        this.presentToast('Pedido Finalizado com sucesso!');
      });
    },  2000);
  }

  async presentLoading() {
    this.loading = await this.loadingController.create({ message: 'Tudo Certo em alguns minutos chega...'});
    return this.loading.present();
  }
  
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }
}
