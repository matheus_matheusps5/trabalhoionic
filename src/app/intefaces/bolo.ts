export interface Bolo {
    id?: number;
    nome?: string;
    descricao?: string;
    imagem?: string;
    preco?: number;
}
