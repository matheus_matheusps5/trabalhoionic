import { Component, OnInit } from '@angular/core';
import { Bolo } from '../intefaces/bolo';
import { BolosService } from '../services/bolos.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  
  bolos: any;
  
  constructor(private boloService:BolosService) {}
   
  ngOnInit(){
    this.boloService.getBolos().subscribe( data =>{
      this.bolos = data;
    });
  }
}


