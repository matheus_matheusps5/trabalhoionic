import { Injectable } from '@angular/core';
import { Bolo } from '../intefaces/bolo';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class BolosService {

  constructor(private http: HttpClient) { }


  getBolos(): Observable<Bolo>{
    let host = environment.host;

    return this.http.get(`${host}/appbolos/bolos.php`);
   
  }
}
